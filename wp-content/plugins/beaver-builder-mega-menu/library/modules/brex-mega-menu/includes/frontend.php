<nav>
    <ul class="brex-mega-menu">
        <?php
        // Loop theough the menu items
        for ($i=0; $i<count($settings->menu_items); $i++){
            // Get the current menu item
            $current_menu_item = $settings->menu_items[$i];
            // Get the the type of menu item
            $menu_item_type = $current_menu_item->navigation_item_type;
            // For different types of menus...
            switch( $menu_item_type ){

                // For vanilla dropdowns
                case 'standard_dropdown':
                $module->renderStandardDropdown( 
                    $current_menu_item->navigation_item_name,
                    $current_menu_item->dropdown_items_menu
                );
                break;

                // For mega menus
                case 'mega_menu':
                $module->renderMegaMenu( 
                    $current_menu_item->navigation_item_name, 
                    $current_menu_item->saved_row
                );
                break;

                // For regular links
                case 'simple_link':
                $module->renderSimpleLink( 
                    $current_menu_item->navigation_item_name, 
                    array(
                        'url'       => $current_menu_item->simple_link_url,
                        'no_follow' => $current_menu_item->simple_link_url_nofollow,
                        'target'    => $current_menu_item->simple_link_url_target
                    )
                );
                break;

                case '':
                break;
                // All unknowns
                default:
                error_log( 'Unknown menu type: ' . $menu_item_type );
                break;
            }
        }
        ?>
    </ul>
    <div class="brex-mobile-menu-icon">
        <a>
            <?php
            echo sprintf(
                '<i class="%s"></i>',
                $settings->mobile_menu_icon
            );
            ?>
        </a>
    </div>
</nav>



<?php

add_action( 'wp_footer', function() use ($id, $settings) {
    ?>
    <div class="fl-node-<?php echo $id;?> brex-mobile-menu-container-node">
        <div class="brex-mobile-menu-container brex-mobile-menu-<?php echo $settings->mobile_menu_slide_in_side;?>">
            <?php

            if ( $settings->mobile_menu_slide_enable_close_icon === 'enabled' ){
                echo sprintf(
                    "<div class=\"close-icon\"><i class=\"%s\"></i></div>",
                    $settings->mobile_menu_close_icon
                );
            }
            
            wp_nav_menu( 
                array(
                    'menu'       => $settings->mobile_menu_id,
                    'menu_class' => 'brex-mobile-menu-inner',
                    'echo'       => true,
                    'container'  => false
                )
            )
            ?>
        </div>
    </div>
    <?php
});
